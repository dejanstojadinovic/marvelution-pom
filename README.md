This project features the organisational project model that is used by all Marvelution Projects

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/MARVADMIN>

Continuous Builder
==================
<https://marvelution.atlassian.net/browse/MARVADMIN-POM>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
